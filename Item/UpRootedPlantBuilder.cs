﻿using System.Collections.Generic;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.Replant.Item {
    public class UpRootedPlantBuilder : IItemBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }

        public void Load() {
            Renderer = new UpRootedPlantPainter();
        }

        public Staxel.Items.Item Build(Blob blob, ItemConfiguration configuration, Staxel.Items.Item spare) {
            try {
                if (spare is UpRootedPlant) {
                    if (spare.Configuration != null) {
                        spare.Restore(configuration, blob);
                        return spare;
                    }
                }

                var plant = new UpRootedPlant(this, configuration);
                plant.Restore(configuration, blob);
                return plant;
            } catch (KeyNotFoundException ex) {
                return Staxel.Items.Item.NullItem;
            }
        }

        public UpRootedPlantPainter Renderer;

        public static string KindCode() {
            return "nimbusfox.replant.item.upRootedPlant";
        }

        public string Kind() {
            return KindCode();
        }
    }
}
