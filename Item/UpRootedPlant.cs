﻿using System.Collections.Generic;
using System.Linq;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.Translation;

namespace NimbusFox.Replant.Item {
    public class UpRootedPlant : Staxel.Items.Item {
        private UpRootedPlantBuilder _builder;
        internal TileConfiguration Plant;
        internal uint GrowthStage;
        public static int SIndex = 0;
        public int Index;
        private bool _placementProblem;

        public UpRootedPlant(UpRootedPlantBuilder builder, ItemConfiguration config) : base(builder.Kind()) {
            _builder = builder;
            Configuration = config;
            Index = SIndex++;
        }

        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) { }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (main.DownClick) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var target, out var adjacent)) {
                    if ((target - adjacent).Y == -1) {
                        if (facade.ReadTile(target, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                            entity.Id, out var tile)) {
                            if (CanPlant(tile)) {
                                if (CanPlant(facade, target, adjacent, entity, TileAccessFlags.SynchronousWait)) {
                                    facade.PlaceTile(entity, adjacent, Plant.MakeTile().CloneWithNewVariant(GrowthStage), TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, entity.Id);

                                    entity.Inventory.RemoveItem(new ItemStack(this, 1));
                                }
                            }
                        }
                    }
                }
            }
        }
        protected override void AssignFrom(Staxel.Items.Item item) { }

        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            if (avatar.LookingAtTile(out var target, out var adjacent)) {
                if ((target - adjacent).Y == -1) {
                    if (universe.ReadTile(target, TileAccessFlags.Preview, ChunkFetchKind.LivingWorld,
                        EntityId.NullEntityId, out var tile)) {
                        _placementProblem = !CanPlant(tile) || !CanPlant(universe, target, adjacent, entity, TileAccessFlags.None);
                        previews.Add(adjacent, Plant.MakeTile().CloneWithNewVariant(GrowthStage));
                        return true;
                    }
                }
            }
            return false;
        }

        public override bool HasAssociatedToolComponent(Components components) {
            return false;
        }

        public override bool Same(Staxel.Items.Item item) {
            if (item is UpRootedPlant plant) {
                return Index == plant.Index;
            }
            return false;
        }

        public override ItemRenderer FetchRenderer() {
            return _builder.Renderer;
        }

        public override void Restore(ItemConfiguration configuration, Blob blob) {
            Plant = GameContext.TileDatabase.GetTileConfiguration(blob.GetString("plant"));
            if (blob.Contains("stage")) {
                GrowthStage = (uint)blob.GetLong("stage");
            }
            Index = (int)blob.GetLong("Index", SIndex);
            base.Restore(configuration, blob);
        }

        public override void Store(Blob blob) {
            blob.SetString("plant", Plant.Code);
            blob.SetLong("stage", GrowthStage);
            blob.SetLong("Index", Index);
            base.Store(blob);
        }

        public override string GetItemCode() {
            return Plant.Code;
        }

        public override string GetItemTranslation(LanguageDatabase lang) {
            return ClientContext.LanguageDatabase.GetTranslationString(Plant.Code + ".name");
        }

        public void SetGrowthStage(Tile tile) {
            GrowthStage = tile.Alt();
        }

        private bool CanPlant(Tile tile) {
            return GameContext.FarmingDatabase.IsTilledMaterial(tile.Configuration);
        }

        private bool CanPlant(EntityUniverseFacade facade, Vector3I target, Vector3I adjacent, Entity entity, TileAccessFlags flag) {
            Tile tile;
            var tiles = new Tile[Plant.CompoundSize.X * Plant.CompoundSize.Y * Plant.CompoundSize.Z];
            var tilledTiles = new Tile[Plant.CompoundSize.X * Plant.CompoundSize.Z];

            for (var x = 0; x < Plant.CompoundSize.X; x++) {
                for (var z = 0; z < Plant.CompoundSize.Z; z++) {
                    for (var y = 0; y < Plant.CompoundSize.Y; y++) {
                        if (facade.ReadTile((adjacent - new Vector3I(Plant.CompoundSize.X / 2, 0, Plant.CompoundSize.Z / 2)) + new Vector3I(x, y, z),
                            flag, ChunkFetchKind.LivingWorld, entity.Id,
                            out tile)) {
                            tiles[
                                x + z * Plant.CompoundSize.X +
                                y * Plant.CompoundSize.X * Plant.CompoundSize.Z] = tile;
                        } else {
                            tiles[
                                    x + z * Plant.CompoundSize.X +
                                    y * Plant.CompoundSize.X * Plant.CompoundSize.Z] =
                                GameContext.TileDatabase.GetTileConfiguration(Constants.SkyCode)
                                    .MakeTile();
                        }
                    }

                    if (facade.ReadTile(
                        (target - new Vector3I(Plant.CompoundSize.X / 2, 0, Plant.CompoundSize.Z / 2)) +
                        new Vector3I(x, 0, z), flag, ChunkFetchKind.LivingWorld, entity.Id, out tile)) {
                        tilledTiles[(Plant.CompoundSize.X * z) + x] = tile;
                    }
                }
            }

            return tiles.All(x => x.Configuration.Code == Constants.SkyCode) && tilledTiles.All(CanPlant);
        }

        public override bool TilePlacementProblem() {
            return _placementProblem;
        }
    }
}
