﻿using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Core;
using Staxel.Entities;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.Replant.Item {
    public class ForkItem : Staxel.Items.Item {
        private readonly ForkItemBuilder _builder;

        public ForkItem(ForkItemBuilder builder, ItemConfiguration config) : base(builder.Kind()) {
            _builder = builder;
            Configuration = config;
        }
        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) { }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (main.DownClick) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var target, out _)) {
                    Tile tile;
                    var blob = BlobAllocator.Blob(true);
                    if (facade.FindReadCompoundTileCore(target, TileAccessFlags.SynchronousWait,
                        ChunkFetchKind.LivingWorld, entity.Id, out var core, out tile)) {
                        if (GameContext.PlantDatabase.IsPlant(tile)) {
                            blob.SetString("plant", tile.Configuration.Code);
                            blob.SetString("kind", UpRootedPlantBuilder.KindCode());
                            blob.SetString("code", "nimbusfox.replant.item.uprootedPlant");
                            var item = (UpRootedPlant) GameContext.ItemDatabase.SpawnItem(blob, NullItem);
                            item.SetGrowthStage(tile);
                            ItemEntityBuilder.SpawnDroppedItem(entity, facade, new ItemStack(item, 1), target.ToVector3D(), new Vector3D(0, 1, 0), Vector3D.Zero, SpawnDroppedFlags.AttemptPickup);
                            facade.PlaceTile(entity, core, GameContext.TileDatabase.GetTileConfiguration(Constants.SkyCode).MakeTile(), TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, entity.Id);
                        }
                    }

                    if (facade.ReadTile(target, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, entity.Id,
                        out tile)) {
                        if (GameContext.PlantDatabase.IsPlant(tile)) {
                            blob.SetString("plant", tile.Configuration.Code);
                            blob.SetString("kind", UpRootedPlantBuilder.KindCode());
                            blob.SetString("code", "nimbusfox.replant.item.uprootedPlant");
                            var item = (UpRootedPlant) GameContext.ItemDatabase.SpawnItem(blob, NullItem);
                            item.SetGrowthStage(tile);
                            ItemEntityBuilder.SpawnDroppedItem(entity, facade, new ItemStack(item, 1), target.ToVector3D(), new Vector3D(0, 1, 0), Vector3D.Zero, SpawnDroppedFlags.AttemptPickup);
                            facade.PlaceTile(entity, target, GameContext.TileDatabase.GetTileConfiguration(Constants.SkyCode).MakeTile(), TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, entity.Id);
                        }
                    }

                    Blob.Deallocate(ref blob);
                }
            }
        }

        protected override void AssignFrom(Staxel.Items.Item item) { }
        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            return false;
        }

        public override bool HasAssociatedToolComponent(Components components) {
            return false;
        }

        public override ItemRenderer FetchRenderer() {
            return _builder.Renderer;
        }

        public override bool Same(Staxel.Items.Item item) {
            if (item == NullItem) {
                return false;
            }
            return item.Configuration.Code == Configuration.Code;
        }
    }
}
