﻿using Plukit.Base;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Rendering;

namespace NimbusFox.Replant.Item {
    public class UpRootedPlantPainter : ItemRenderer {
        public override void Render(Staxel.Items.Item item, DeviceContext graphics, ref Matrix4F itemMatrix, Vector3D playerPosition, Vector3D renderOrigin,
            ref Matrix4F matrix, Timestep renderTimestep, string animation, float animationCycle, RenderMode renderMode) {
            if (item is UpRootedPlant plant) {
                var oM = matrix
                    .RotateUnitX(plant.Configuration.InHandRotation.X)
                    .RotateUnitY(plant.Configuration.InHandRotation.Y)
                    .RotateUnitZ(plant.Configuration.InHandRotation.Z)
                    .Multiply(ref itemMatrix);

                var scale = plant.Plant.ItemConfiguration.InHandScale;

                if (scale.X == 0) {
                    scale.X = 0.2f;
                }

                if (scale.Y == 0) {
                    scale.Y = 0.2f;
                }

                if (scale.Z == 0) {
                    scale.Z = 0.2f;
                }

                plant.Plant.GetDrawable(1, plant.GrowthStage, Vector3I.Zero, Vector3F.Zero, ref itemMatrix, out _).Scale(scale).Render(graphics, ref oM);
            }
        }

        public override void RenderIcon(Staxel.Items.Item item, DeviceContext graphics, ref Matrix4F itemMatix) {
            if (item is UpRootedPlant plant) {
                var oM = Matrix4F.Identity
                    .Multiply(ref itemMatix);

                plant.Plant.GetDrawable(1, plant.GrowthStage, Vector3I.Zero, Vector3F.Zero, ref itemMatix, out _).Scale(plant.Plant.ItemConfiguration.IconScale).Render(graphics, ref oM);
            }
        }

        public override void RenderInWorldFullSized(Staxel.Items.Item item, DeviceContext graphics, ref Matrix4F matrix, bool compact, bool secondary) {
            if (item is UpRootedPlant plant) {
                var oM = Matrix4F.Identity
                    .Multiply(ref matrix);
                plant.Plant.GetDrawable(1, plant.GrowthStage, Vector3I.Zero, Vector3F.Zero, ref matrix, out _).Render(graphics, ref oM);
            }
        }
    }
}
