﻿using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.Replant.Item {
    public class ForkItemBuilder : IItemBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }

        public void Load() {
            Renderer = new ItemRenderer();
        }
        public Staxel.Items.Item Build(Blob blob, ItemConfiguration configuration, Staxel.Items.Item spare) {
            return new ForkItem(this, configuration);
        }

        public ItemRenderer Renderer;

        public static string KindCode() {
            return "nimbusfox.replant.item.fork";
        }

        public string Kind() {
            return KindCode();
        }
    }
}
